CREATE TABLE salary (
    id INTEGER NOT NULL,
	user_id INTEGER,
	data_increase DATE,
	salary NUMERIC(10, 2),
	description VARCHAR,
	PRIMARY KEY (id),
	FOREIGN KEY(user_id) REFERENCES users (id)
);

CREATE TABLE users (
	id INTEGER NOT NULL,
	email TEXT,
	username TEXT,
	password_hash TEXT,
	PRIMARY KEY (id),
	UNIQUE (email),
	UNIQUE (username)
);