from datetime import date
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel


class AboutSalaryBase(BaseModel):
    data_increase: date
    salary: Decimal
    description: Optional[str]


class AboutSalary(AboutSalaryBase):
    id: int

    class Config:
        orm_mode = True


class AboutSalaryCreate(AboutSalaryBase):
    pass
