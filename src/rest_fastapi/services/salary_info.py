from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session
from .. import tabels
from typing import List
from ..database import get_session
from ..models.salary_info import AboutSalaryCreate


class AboutSalaryService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get(self, user_id: int, salary_info_id: int) -> tabels.AboutSalary:
        salary_info = (
            self.session
                .query(tabels.AboutSalary)
                .filter_by(
                    id=salary_info_id,
                    user_id=user_id,
            )
            .first()
        )
        if not salary_info:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return salary_info

    def get_list(self, user_id: int,) -> List[tabels.AboutSalary]:
        query = (
            self.session
            .query(tabels.AboutSalary)
            .filter_by(user_id=user_id)
        )
        salary_info = query.all()
        return salary_info

    def get(self, user_id: int, salary_info_id: int) -> tabels.AboutSalary:
        return self._get(user_id, salary_info_id)

    def create(self, user_id: int, salary_data: AboutSalaryCreate) -> tabels.AboutSalary:
        salary_info = tabels.AboutSalary(
            **salary_data.dict(),
            user_id=user_id,
        )
        self.session.add(salary_info)
        self.session.commit()
        return salary_info

    def delete(self, user_id: int,salary_info_id: int):
        salary_info = self._get(user_id, salary_info_id)
        self.session.delete(salary_info)
        self.session.commit()
