from fastapi import APIRouter, Depends, Response, status
from typing import List
from ..models.auth import User
from ..models.salary_info import AboutSalary, AboutSalaryCreate
from ..services.salary_info import AboutSalaryService
from ..services.auth import get_current_user
router = APIRouter(
    prefix='/salary_info',
)


@router.get('/', response_model=List[AboutSalary])
def get_salary_info(
    user: User = Depends(get_current_user),
    service: AboutSalaryService = Depends(),
):
    return service.get_list(user_id=user.id)


@router.post('/', response_model=AboutSalary)
def create_salary_info(
        salary_data: AboutSalaryCreate,
        user: User = Depends(get_current_user),
        service: AboutSalaryService = Depends(),
):
    return service.create(user_id=user.id, salary_data=salary_data)


# @router.get('/{salary_info_id}', response_model=AboutSalary)
# def get_salary_info(
#         salary_info_id: int,
#         user: User = Depends(get_current_user),
#         service: AboutSalaryService = Depends(),
# ):
#     return service.get(user_id=user.id, salary_info_id=salary_info_id)


@router.delete('/{salary_info_id}', response_model=AboutSalary)
def delete_salary_info(
        salary_info_id: int,
        user: User = Depends(get_current_user),
        service: AboutSalaryService = Depends(),
):
    service.delete(user_id=user.id, salary_info_id=salary_info_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
