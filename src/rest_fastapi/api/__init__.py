from fastapi import APIRouter
from .auth import router as auth_router
from .salary_info import router as salary_info_router

router = APIRouter()
router.include_router(auth_router)
router.include_router(salary_info_router)
