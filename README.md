# **ЦФТ тестовое задание**
###Инструкция запуска приложения
1. `python3 -m venv venv`
2. `pip install poetry`
3. `poetry install`
4. Если база не иницализирована, то выполнить в ней скрипты в init.sql
5. **Запуск сервиса `PYTHONPATH=src python3 -m src.rest_fastapi`**


##Документация API
### Есть в /dosc
####POST
* `/auth/sign-up` - регистрация 
##### Входные данные:
```
{
  "email": "string",
  "username": "string",
  "password": "string"
}
```
##### Входные данные:
```
{
  "access_token": "string",
  "token_type": "bearer"
}
```
####POST
* `/auth/sign-in` - вход по логику и паролю 
##### Входные данные:
```
{
  "username": "string",
  "password": "string"
}
```
##### Входные данные:
```
{
  "access_token": "string",
  "token_type": "bearer"
}
```
####GET
* `/auth/user` - информация о пользователе  
##### Входные данные:
Необходима авторизация или регистрация через Bearer jwt_token
##### Входные данные:
```
{
  "email": "string",
  "username": "string",
  "id": 0
}
```
####GET
* `/salary_info/` - получить информацию о зп
##### Входные данные:
Необходима авторизация или регистрация через Bearer jwt_token
##### Выходные данные:
```
[
  {
    "data_increase": "2023-06-12",
    "salary": 0,
    "description": "string",
    "id": 0
  }
]
```
####POST
* `/salary_info/` - добавить информацию о зп
##### Входные данные:
Необходима авторизация или регистрация через Bearer jwt_token
```
{
  "data_increase": "2023-06-12",
  "salary": 0,
  "description": "string"
}
```
##### Выходные данные:
```
{
  "data_increase": "2023-06-12",
  "salary": 0,
  "description": "string",
  "id": 0
}
```
####DELETE
* `/salary_info/{salary_info_id}/` - удаление данных о зп юзера по зп_id 
##### Входные данные:
Необходима авторизация или регистрация через Bearer jwt_token  
ЗП_id, которую хочет удалить пользователь
```
{
  "salary_info_id": "ЗП_id"
}
```
##### Выходные данные:
```
{
  "data_increase": "2023-06-12",
  "salary": 0,
  "description": "string",
  "id": 0
}
```

## *структура проекта:*
5. В директории src основные файлы проекта
6. src/rest_fastapi/database.py создание сессии для БД, работа с базой
7. src/rest_fastapi/tabels.py модель таблицы зарплаты и модель таблицы юзера для БД
8. src/rest_fastapi/settings.py Настройки сервера, jwt токена
9. src/rest_fastapi/app.py заспуск приложения
###  rest_fastapi/api
10. src/rest_fastapi/api/__init__.py корневой роутер, для логического разделения проекта
11. src/rest_fastapi/api/salary_info.py обработчик запроса о зп
12. src/rest_fastapi/api/auth.py обработчик запроса об авторизации
### rest_fastapi/models
13. src/rest_fastapi/models/salary_info.py описание моделей данных о зп
14. src/rest_fastapi/models/auth.py описание моделей данных для авторизации и создании пользователей 
### rest_fastapi/services
15. src/rest_fastapi/services/salary_info.py логика запросов о зп
16. src/rest_fastapi/services/auth.py локгика запросов об авторизации